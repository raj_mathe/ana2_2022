# Analysis II für Mathematiker, SoSe 2022 #

Diese Repository ist für die Übungsgruppe am Mittwoch.

**HINWEIS:** In diesem Repository werden keine persönlichen Daten gespeichert.

In diesem Repository findet man:

- Protokolle der Übungsgruppe [hier](./protocol).
- Notizen [hier](./notes).
- Symbolverzeichnis unter [notes/glossar.md](./notes/glossar.md).
- Referenzen unter [notes/quellen.md](./notes/quellen.md).
