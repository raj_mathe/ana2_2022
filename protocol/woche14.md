# Vorlesungswoche 11 (4.–10. Juli 2022) #

## Agenda ##

- [ ] Rückgabe der ÜB
- [ ] ÜB13 / Vorrechnen
- [ ] Besprechung von verwandten Themen: lokale Umkehrbarkeit, Satz der impl. Funktion, usw.
