# Vorlesungswoche 8 (23.–29. Mai 2022) #

## Agenda ##

- [x] Rückgabe der ÜB
- [x] ÜB7 / Vorrechnen
- [x] Besprechung von Themen in ÜB8
  - zusammenhängend
  - wegzusammenhängend
  - Konstruktion von stetigen Fkt
  - Hinweise zu Aufgaben.
  - Literatur zu A4.

## Nächste Woche ##

- Übungsblatt 8 + Vorrechnen (**Beachtet:** Kopie von Abgaben behalten!)

### TODOs (Studierende) ###

- Kapitel 13 anfangen; Hauptbegriffe und -resultate auflisten und durchgehen.
- ÜB8 bis Frist abgeben.
