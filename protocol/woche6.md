# Vorlesungswoche 6 (9.–15. Mai 2022) #

## Agenda ##

- [ ] Rückgabe der ÜB
- [ ] Theorie wiederholen
  - [ ] Axiome einer Metrik
  - [ ] Stetigkeit einer Fkt zw. zwei metrischen Räumen
    - in einem gegebenen Punkt
    - überall (vgl. [**VL, Lemma 25 + Thm 57, Seite 185**])
- [ ] ÜB5 / Vorrechnen
- [ ] Besprechung von Themen in ÜB6
  - [ ] das Innere, der Abschluss, der Rand
  - [ ] Produktopologie
  - [ ] Stetigkeit bzgl. was? — zugrunde liegende Konvergenzbegriff der Topologie vs. Metrik.

## Nächste Woche ##

- Übungsblatt 6 + Vorrechnen (**Beachtet:** Kopie von Abgaben behalten!)

### TODOs (Studierende) ###

- Kapitel 12 durchlesen.
- ÜB6 bis Frist abgeben.
