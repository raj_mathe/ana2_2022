# Vorlesungswoche 5 (2.–8. Mai 2022) #

## Agenda ##

- [x] Rückgabe der ÜB
- [x] Umgang mit Kapitel 12
  - [x] Begriffe für metr. Räume
  - [x] Verallg. zu top. Räumen
  - [x] eine Metrik vs. „die“ Metrik
  - [x] Umgebungen und Stetigkeit
- [x] Aufgaben: ÜB4
  - [x] Abschluss der offenen Kugel ⊆ die abgeschlossene Kugel; und nicht immer =!
- [x] Fragen zu ÜB5

## Nächste Woche ##

- Übungsblatt 5 + Vorrechnen (**Beachtet:** Kopie von Abgaben behalten!)

### TODOs (Studierende) ###

- Kapitel 12 durchlesen. Tipps:
  - Zusammenfassung für sich erstellen.
  - **Achtung:** Beim Thema Topologie gibt es viele Überschneidungen
    und viel Potential für Verwechselung (z. B. „falsche Freunde“)
  - Themen sortieren und unterordnen, damit alles leichter einzuordnen ist und nicht überwältigend.
- ÜB5 bis Frist abgeben.
