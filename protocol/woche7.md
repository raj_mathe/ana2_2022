# Vorlesungswoche 7 (16.–22. Mai 2022) #

## Agenda ##

- [x] Rückgabe der ÜB
- [x] Theorie wiederholen
  - [x] Stetigkeit: Komposition
  - [x] Inneres, Abschluss, Rand von Mengen/Kugeln
  - [x] Kompaktheit
  - [ ] Produkttopologie
- [ ] ÜB6 / Vorrechnen
- [ ] Besprechung von Themen in ÜB7

## Nächste Woche ##

- Übungsblatt 7 + Vorrechnen (**Beachtet:** Kopie von Abgaben behalten!)

### TODOs (Studierende) ###

- Kapitel 12 nochmals drüber gehen; zusammenfassen; ggf. Checkliste von „TODOs“ erstellen.
- ÜB7 bis Frist abgeben.
