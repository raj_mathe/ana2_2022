# Vorlesungswoche 2 (11.—17. April 2022) #

## Agenda ##

- [x] Organisatorisches
  - Bedenke auch, zur Vertiefung sich die Literatur (referenziert in [protocol/woche1.md](./woche1.md)) nachzuschlagen.
- [x] Aufgaben: ÜB1
  - A1: Hauptidee war:
    - g´(a') = ƒ´(a') - ξ < 0 < ƒ´(b') - ξ = g´(b')
    - g diffbar auf (a', b') und rechts- bzw. linksableitbar im Pkt a' bzw. b'
    - Für solche Funktionen gilt das Lemma:
      - g stetig auf [a', b'] und besitzt damit ein globales Minimum (c,g(c))
      - c ≠ a', weil sonst die Rechtsableitung g´(a') ≥ 0 gelten muss.
      - c ≠ b', weil sonst die Linksableitung g´(b') ≤ 0 gelten muss.
      - Darum c ∈ (a', b'). Also:
        ```
        g´(c) = Rechtsableitung g´(c)
              = lim (g(x)-g(c))/(x-c) mit x ⟶ γ⁺
              = lim |g(x)-g(c)|/|x-γ| da x > γ und g(x) ≥ g(c)
              ≥ 0
        g´(c) = Linksableitung g´(c)
              = lim (g(x)-g(c))/(x-c) mit x ⟶ c¯
              = lim -|g(x)-g(c)|/|x-c| da x < c und g(x) ≥ g(c)
              = - lim |g(x)-g(c)|/|x-c| mit x ⟶ c¯
              ≤ 0
        ⟹ g´(c) = 0.
        ```
  - A2 übersprungen.
  - A3 vorgerechnet
  - A4 mit der Variante berechnet --- mit offenen Intervallen.
    </br>
    Hier eine Variante mit abgeschlossenen Intervallen:
    ```
    | Sei n ≥ 1 eine beliebige Zahl.
    | Setze:
    |   A := { x ∈ [0,1] | N(x) ≥ 1/n }
    |      = { x ∈ [0,1] | x ∈ ℚ \ {0} und hat Form p/q mit 1 ≤ p ≤ q und 1/q = N(x) ≥ 1/n }
    |      = { x ∈ [0,1] | x ∈ ℚ \ {0} und hat Form p/q mit 1 ≤ p ≤ q ≤ n }
    | Also ist A endlich, da offensichtlich |A| ≤ n².
    |
    | | Sei Z = (x[0], x[1], ..., x[m]) eine bel. Zerlegung.
    | | Wegen Überschneidungen durch die Endpunkte
    | | enthalten höchstens 2·|A| Intervalle Punkte aus A.
    | |
    | | - Für diese „bad“ Intervalle gelten
    | |     sup_{x ∈ [x[k], x[k+1]]} N(x) ≤ sup_{x ∈ [0, 1]} N(x) = 1
    | |     inf_{x ∈ [x[k], x[k+1]]} N(x) ≥ inf_{x ∈ [0, 1]} N(x) = 0
    | | - Für die m - 2·|A| anderen „good“ Intervalle gelten
    | |     sup_{x ∈ [x[k], x[k+1]]} N(x) ≤ 1/(n+1) < 1/n
    | |     inf_{x ∈ [x[k], x[k+1]]} N(x) ≥ inf_{x ∈ [0, 1]} N(x) = 0
    | |
    | | Darum:
    | |
    | |  U_Z(N) = ∑_{k=0}^{m-1} inf_{x ∈ [x[k], x[k+1]]} N(x) · (x[k+1] - x[k])
    | |    ≥ ∑_{k=0}^{m-1} 0 · (x[k+1] - x[k])
    | |    = 0.
    | |
    | |  O_Z(N) = ∑_{k=0}^{m-1} sup_{x ∈ [x[k], x[k+1]]} N(x) · (x[k+1] - x[k])
    | |    = ∑_{k „bad“} sup_{x ∈ [x[k], x[k+1]]} N(x) · (x[k+1] - x[k])
    | |      + ∑_{k „good“} sup_{x ∈ [x[k], x[k+1]]} N(x) · (x[k+1] - x[k])
    | |    ≤ ∑_{k „bad“} 1 · (x[k+1] - x[k])
    | |      + ∑_{k „good“} 1/n · (x[k+1] - x[k])
    | |    ≤ ∑_{k „bad“} 1 · (x[k+1] - x[k])
    | |      + ∑_{k=0}^{m-1} 1/n · (x[k+1] - x[k]) <--- Teleskopsumme = 1/n
    | |    ≤ |Z|·#bad + 1/n
    | |    ≤ |Z|·2|A| + 1/n
    | |________________________________
    | ⟹ Für fixes n, aber immer feiner werdende Zerlegungen Z:
    |
    |  lim_Z O_Z(N) = limsup_Z O_Z(N)
    |    ≤ limsup_Z 2|A|·|Z| + 1/n
    |    = 2|A|·0 + 1/n
    |    = 1/n
    |________________________________
    ⟹ Da n beliebig gewählt werden kann, gilt somit:

    inf_Z O_Z(N) = lim_Z O_Z(N)
        ≤ inf_n 1/n
        = 0.

    Also: 0 ≤ sup_Z U_Z(N) ≤ inf_Z O_Z(N) ≤ 0
    Also: sup_Z U_Z(N) = inf_Z O_Z(N) = 0.
    Also: N integrierbar und ∫ N dx = 0.
    ```
- [x] Zusatz: Fragen zu ÜB2

## Nächste Woche ##

- Übungsblatt 2 + Vorrechnen (**Beachtet:** Kopie von Abgaben behalten!)

### TODOs (Studierende) ###

- Kapitel 10 weiter vertiefen durchgehen, inbes. die Konzepte aus LinAlg durchgehen.
- ÜB2 bis Frist abgeben.
