# Vorlesungswoche 9 (30. Mai – 5. Juni 2022) #

## Agenda ##

- [ ] Rückgabe der ÜB
- [x] ÜB8 / Vorrechnen
- [x] Besprechung von Themen in ÜB9

## Nächste Woche ##

- Übungsblatt 9 + Vorrechnen (**Beachtet:** Kopie von Abgaben behalten!)

### TODOs (Studierende) ###

- Kapitel 13 verschiedene Konzepte von Stetigkeit + Differenzierbarkeit erlernen
  - Implikationen (und wenn sie strikt sind!) verinnerlichen
  - auf „Überschneidungen“ achten
- ÜB9 bis Frist abgeben.
