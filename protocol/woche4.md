# Vorlesungswoche 4 (25. April — 1. Mai 2022) #

## Agenda ##

- [x] Organisatorisches / Rückgabe der ÜB
- [x] Aufgaben: ÜB3
  - siehe auch [./notes/notes.pdf](../notes/notes.pdf) für A1 + 4.
  - A3 (a) geht auch einfacher,
    wenn man `∫ sin(x)/x dx` von `x=0` bis `x=+∞` untersucht.
    In einem anderen Modul wurde dieses Integral explizit bestimmt.
    Aus der Endlichkeit dieses Integrals,
    sowie der Riemann-Integrierbarkeit von dem Integral von `0` bis `1`
    (man braucht nur die Stetigkeit einer »passenden« Fortsetzung der Funktion),
    folgt die uneigentliche Riemann-Integrierbarkeit von dem Integral von `1` bis `+∞`.
- [x] Fragen zu ÜB4

## Nächste Woche ##

- Übungsblatt 4 + Vorrechnen (**Beachtet:** Kopie von Abgaben behalten!)

### TODOs (Studierende) ###

- Kapitel 11 + 12 durchlesen.
- ÜB4 bis Frist abgeben.
