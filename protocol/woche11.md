# Vorlesungswoche 11 (13.–19. Juni 2022) #

## Agenda ##

- [x] Rückgabe der ÜB
- [x] ÜB10 / Vorrechnen
- [x] Besprechung von Themen in ÜB11

## Nächste Woche ##

- Übungsblatt 11 + Vorrechnen (**Beachtet:** Kopie von Abgaben behalten!)

### TODOs (Studierende) ###

- Kapitel 13 verschiedene zusammenfassen.
- ÜB11 bis Frist abgeben.
