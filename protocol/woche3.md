# Vorlesungswoche 3 (18.—24. April 2022) #

## Agenda ##

- [x] Organisatorisches
- [x] Aufgaben: ÜB2
  - alles bis auf Zusatzaufgabe
  - siehe auch [./notes/notes.pdf](../notes/notes.pdf) für A1 + 4.
- [x] Fragen zu ÜB3

## Nächste Woche ##

- Übungsblatt 3 + Vorrechnen (**Beachtet:** Kopie von Abgaben behalten!)

### TODOs (Studierende) ###

- Jede sollte für sich Hauptkonzepte aus Kapitel 10 für sich zusammenfassen, Wissenslücken vorgreifen. Z. B.
  - Zerlegungen, Stützpunkte, Treppenfunktionen, untere/obere Summen
  - _hinreichende_ Bedingungen für Riemann-Integrierbarkeit
  - Resultate wie gl. Konvergenz R-int Fkt sind R-int
  - Cauchy-Schwarz
  - uneigentliche Integrale
  - _et cetera_
- ÜB3 bis Frist abgeben.
