# Vorlesungswoche 1 (4.—10. April 2022) #

## Agenda ##

- [x] Organisatorisches
  - Takt
    - Kopie von Abgaben behalten
  - Tipps zum Lernen mit Skript + Übungen + zur laufenden Klausurvorbereitung
  - Ressourcen, z. B.
    - Deitmar <https://link.springer.com/book/10.1007/978-3-642-54810-9> (offiziell, gratis PDF)
    - Forster <https://link.springer.com/book/10.1007/978-3-658-11545-6> (offiziell, gratis PDF)
  - Dieses Git-Repo
- ~~[ ] Aufgaben: Klausuraufgaben~~~
- [x] Konzepte (Zerlegungen, obere Summen, untere Summen) hinter Riemann-Integral
  - In den [Handnotizen](../notes/woche1.pdf) findet man ein paar Ergänzungen sowie das Beispiel am Ende der ÜG (komplett durchgerechnet).
  - Unter [diesem Link](https://www.geogebra.org/m/bnhwycf4) findet man ein interaktives Diagramm,
    das die Konzepte von Zerlegungen + unteren/oberen Summen demonstriert.

## Nächste Woche ##

- Übungsblatt 1 + Vorrechnen (**Beachtet:** Kopie von Abgaben behalten!)

### TODOs (Studierende) ###

- Kapitel 10 durchgehen.
- ÜB1 bis Frist abgeben.
