# Vorlesungswoche 10 (6.–12. Juni 2022) #

## Agenda ##

- [x] Rückgabe der ÜB
- [x] ÜB9 / Vorrechnen
- [x] Besprechung von Themen in ÜB10

## Nächste Woche ##

- Übungsblatt 10 + Vorrechnen (**Beachtet:** Kopie von Abgaben behalten!)

### TODOs (Studierende) ###

- Kapitel 13 verschiedene Konzepte von Stetigkeit + Differenzierbarkeit erlernen
  - Implikationen (und wenn sie strikt sind!) verinnerlichen
  - auf „Überschneidungen“ achten
- ÜB10 bis Frist abgeben.
